﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lab08
{
    public partial class fTablet : Form
    {
        public Tablet TheTablet;
        public fTablet(Tablet t)
        {
            TheTablet= t;
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            TheTablet.Name = tbColour.Text.Trim();
            TheTablet.Manufacturer = tbManufacturer.Text.Trim();
            TheTablet.Colour = tbName.Text.Trim();
            TheTablet.Memory = int.Parse(tbMemory.Text.Trim());
            TheTablet.Amount = int.Parse(tbAmount.Text.Trim());
            TheTablet.Cost = double.Parse(tbCost.Text.Trim());
            TheTablet.Users = int.Parse(tbUsers.Text.Trim());
            DialogResult = DialogResult.OK; 
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void tTablet_Load(object sender, EventArgs e)
        {
            if (TheTablet != null)
            {
                tbColour.Text = TheTablet.Name;
                tbManufacturer.Text = TheTablet.Manufacturer;
                tbName.Text = TheTablet.Colour;
                tbMemory.Text = TheTablet.Memory.ToString();
                tbAmount.Text = TheTablet.Amount.ToString();
                tbCost.Text = TheTablet.Cost.ToString("0.00");
                tbUsers.Text = TheTablet.Users.ToString();
            } 
        }
    }
}
